import React, {Component} from 'react'
import './table.css';
export class Table extends Component{
       render() {
        const header = this.props.config.map((elem) => {
            return(
                <th key={elem.id}>{elem.titulo}</th>
            );

        })
        const body = this.props.data.map((persona) => {
            const fila = this.props.config.map((elem) => {
                return (
                    <td key={elem.id}>{persona[elem.field]}</td>
                );
            });

            return (
                <tr key={persona.id}>{fila}</tr>
            )
        })
        return (
            <div className="table">

                <div className="header">Users</div>
                <table>
                    <thead>
                        <tr>
                            {header}
                        </tr>
                    </thead>
                    <tbody>
                        {body}
                    </tbody>

                </table>

            </div>
        );
    }

}