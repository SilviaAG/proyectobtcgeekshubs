import React, { Component } from 'react';
import './App.css';
import {Table} from './table/table';

class App extends Component {
    constructor (props) {
        super (props);
        this.state = {
            config: [
                {
                    titulo: 'Name',
                    field: 'name',
                    id: 0
                },
                {
                    titulo: 'Surname',
                    field: 'surname',
                    id: 1
                },
                {
                    titulo: 'Role',
                    field: 'role',
                    id: 2
                },
                {
                    titulo: 'Headquarters',
                    field: 'headquarters',
                    id: 3
                }

            ],
            data:[
                {
                    name: 'Mariano',
                    surname: 'Rajoy',
                    role: 'Registrador',
                    headquarters: 'Estepona',
                    id: 0
                },
                {
                    name: 'Jose María',
                    surname: 'Aznar',
                    role: 'Oráculo',
                    headquarters: 'Oropesa',
                    id: 1
                },
                {
                    name: 'Felipe',
                    surname: 'González',
                    role: 'Jarrón chino',
                    headquarters: 'Boadilla',
                    id: 2
                },
                {
                    name: 'Manuel',
                    surname: 'Fraga',
                    role: 'Lo que surja',
                    headquarters: 'Santiago',
                    id: 3
                },

            ]
        };

    }
  render() {
    return (
        <div className="page">
        <div className="header">
            <nav>
                <ul className="navigation">
                    <li className="navButton"><a href="">Teams</a></li>
                    <li className="navButton"><a href="">Ideas</a></li>
                    <li className="navButton"><a href="">Cities</a></li>
                    <li className="navButton"><a href="">Users</a></li>
                    <li className="floatLeft">Demium.</li>
                </ul>
            </nav>
        </div>
        <div className="body"> <Table config={this.state.config} data={this.state.data}/> </div>
        <div className="footer">
            <p>&copy; Demium. All rights reserved</p>
        </div>
    </div>


    );
  }
}

export default App;
